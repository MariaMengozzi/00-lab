package model;

public class Person {
    private String name;
    private int age;
    private String surname;

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public void updateAge(int newAge) {
        this.age = newAge;
    }

    public void setSurname(String surname){
        this.surname = surname;
    }

    public String getSurname(){
        return this.surname;
    }

    @Override
    public String toString(){
        return this.name;
    }
}
